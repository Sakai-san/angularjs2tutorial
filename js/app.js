var app = angular.module('tutorial', ['ngRoute']);

app.config( function($routeProvider){
    $routeProvider
        .when( '/', {
        	templateUrl: 'partials/pictures.html',
        	controller : 'pictureController'
        })
        .when('/detail/:id', {
        	templateUrl: 'partials/detail.html',
        	controller : 'detailController'
        })
        .when('/upload', {
        	templateUrl: 'partials/upload.html',
        	controller: 'uploadController'
        })
        .otherwise( {
        	redirectTo: '/'
        })
});