app.directive('ngFeedback', function($rootScope){
	return {
		scope:{
			localUploadSucceeded : "=succ",
			localMessage : "=mess"
		},
		restrict: 'E',
		link: function(scope, element, attrs) {

			scope.$watch( 'localMessage', function(newVal){
				if(newVal){
					element.text( scope.localMessage );
					if( scope.localUploadSucceeded ){
						element.css("color", "green" );						
					}
					else{
						element.css("color", "red" );				
					}
				}
		    });

		}
	}
}); 
